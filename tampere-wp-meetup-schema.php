<?php
/*
 * @wordpress-plugin
 *
 * Plugin Name: Tampere WordPress Meetup Schema Plugin
 * Plugin URI: https://leodandesign.com
 * Description: Create a schema object on posts and embed json-ld into html
 * Version: 0.1
 * Author: Ben Matthews
 * Author URI: https://leodandesign.com
 */

namespace tampere_meetup;

new schema();

class schema{
    public function __construct(){
        // Set Plugin paths and URIs
        define( 'TWP_PLUGIN_DIR', dirname(__FILE__));
        define( 'TWP_PLUGIN_URL', plugin_dir_url( __FILE__ ));
        // Check To See If ACF is installed
        if (!class_exists('ACF')) {
            // Define path and URL to the ACF plugin.
            define('MY_ACF_PATH', TWP_PLUGIN_DIR . '/includes/acf/');
            define('MY_ACF_URL', TWP_PLUGIN_URL . '/includes/acf/');

            // Include the ACF plugin.
            include_once(MY_ACF_PATH . 'acf.php');

            // Customize the url setting to fix incorrect asset URLs.
            add_filter('acf/settings/url', array($this, 'my_acf_settings_url'));
        }

        // Add the schema fields using ACF
        $this->fields();

        // Set filter to add the json object to the html - add it to the end of the_content - you could use the new wp_body_open hook in themes compatible with WP version 5.2
        add_filter('the_content', array($this, 'schema'), 99);
    }

    public function my_acf_settings_url($url){
        return MY_ACF_URL;
    }

    public function fields(){

    }

    public function schema($content){
        if(!is_admin() && is_singular('post')){
            $this->json = $this->make_json();
            $content .= $this->embed_json();
        }
        return $content;
    }

    private function make_json(){
        $schema = array(
            '@context' => 'https://schema.org',
            '@type' => 'Article',
            'mainEntityOfPage' => array(
                '@type' => 'WebPage',
                '@id' => get_the_permalink()
            ),
            'headline' => get_the_title(),
            'image' => get_the_post_thumbnail_url(),
            'articleBody' => get_the_content(),
            'datePublished' => get_the_date(),
            'dateModified' => get_the_modified_date(),
            'author' => array(
                '@type' => 'Person',
                'name' => get_the_author_meta('display_name')
            ),
            'publisher' => array(
                '@type' => 'Organization',
                'name' => get_bloginfo('title'),
                'logo' => array(
                    '@type' => 'ImageObject',
                    'url' => wp_get_attachment_url( get_theme_mod( 'custom_logo' ))
                ),
            )
        );
        return json_encode($schema);
    }

    private function embed_json(){
        return '<script type="application/ld+json">'. $this->json . '</script>';
    }
}